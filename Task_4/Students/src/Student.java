import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Student {
   private int Student_id;
   private String firstName;
   private String lastName;
   private String DOB;
   
   SimpleDateFormat format_date=new SimpleDateFormat("dd/mm/yyyy");
   
   public Student(int student_id, String firstName, String lastName, String dOB) {
	 super();
	 Student_id = student_id;
	 this.firstName = firstName;
	 this.lastName = lastName;
	 DOB = dOB;
   }
   
   
   public String getDayOfBirthday() {
	   
	   try {
		format_date.parse(DOB);
	  } catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	  }
	   return DOB;
   }


   public int getStudent_id() {
	return Student_id;
   }


   public String getFirstName() {
	return firstName;
   }


  public String getLastName() {
	return lastName;
  }


  public String getDOB() {
	return DOB;
  }
   
   
   
   
}
