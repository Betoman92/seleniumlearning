
import java.util.Random;
public class StudentImplement {

	public static void main(String[] args) {
     
	Random random=new Random();	
	int random_number = 40;	
	int id_student1 = random.nextInt(random_number);
	int id_student2 = random.nextInt(random_number);
	int id_student3 = random.nextInt(random_number);
	
       Student s1=new Student(id_student1, "Jorge", "Palmero", "9/10/1992");
       Student s2=new Student(id_student2, "Alma","Vargas Alonso","8/09/1996");
       Student s3=new Student(id_student3,"Berenice","Vargas Perez","5/07/1992");
       
       System.out.println("Student: "+"ID: "+s1.getStudent_id()+" Name: "+s1.getFirstName()+" LastName: "+s1.getLastName()+" Birthday: "+s1.getDayOfBirthday());
       System.out.println("Student: "+"ID: "+s2.getStudent_id()+" Name: "+s2.getFirstName()+" LastName  "+s2.getLastName()+" Birthday: "+s2.getDayOfBirthday());
       System.out.println("Student: "+"ID: "+s3.getStudent_id()+" Name: "+s3.getFirstName()+" LastName: "+s3.getLastName()+" Birthday: "+s3.getDayOfBirthday());
       
	}
}
